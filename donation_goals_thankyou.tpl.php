<?php
/**
 * @file
 * Template for the "Thank-you" page that users see after making a donation.
 * The following variables can be used:
 *
 * $node (object) - The node that was donated to
 * $donation_goal (object) - Details of the donation goal:
 * $currency - The three-letter code of the currency (e.g. USD)
 * $currency_name - The full name of the currency
 * $currency_symbol - The symbol of the currency
 * $mail - The email address payments are made to
 * $amount - The target amount of the goal (with currency symbol)
 * $amount_saved - The amount of money saved so far (with currency symbol)
 * $message - The formatted donation message.
 * $name: Themed username of node author output from theme_username().
 * $node_url - Direct url of the current node.
 * $links: Themed link back to the node (and possibly other links).
 */
?>
<p><?php print $message; ?></p>

<?php print $links; ?>
