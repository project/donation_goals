
Donation Goals Readme:
----------------------

The Donation Goals module lets you create nodes of the type "Donation Goal."
These goals allow you to set an amount of money you'll need for whatever you're
saving up for. Visitors of the site can then donate money through PayPal. Once
the target amount is reached, the donation form is hidden. If you're putting in
some money yourself, you can easily just update the amount you've saved without
having to donate through PayPal yourself - saving you any fees.

Using permissions it's also possible to deny donations from users. This may be
useful if you don't accept donations, but just want to show how well you're
saving up to get that big thing from your wishlist. Or you can make sure only
authenticated users make a donation.

Requirements:
-------------
Simple Paypal Framework (http://drupal.org/project/simple_paypal).
Required for the module's core functionality.

Views (http://drupal.org/project/views)
Not required, but highly recommended to enable donor overviews etc. Donation
Goals will work fine without this, but it will not be able to show very
detailed information about received donations.

Installation:
-------------
First, make sure you have the Simple Paypal Framework module installed. Also
install Views to get overviews of donors. You can always install Views later if
you want such functionality after all.

Install the module as normal, by unpacking it to the appropriate directory and
enabling it in Administer > Site Building > Modules.

Next, go to Administer > Site Configuration > Donation Goals, and adjust the
settings to your wishes. The default view that ships with this module will use
the "Position of currency symbol" setting until you override the view for the
necessary fields. Custom views will not be updated automatically if you change
this setting later.

Finally, set the permissions at Administer > User Management > Permissions.
Take especially care of the "make donation to any goal" permission! Keeping this
disabled for all users, which is the default, will not allow anybody to make
donations!

Usage:
------
Under "Create Content" you'll find a new node type, Donation Goal. Enter the
title ("Goal") you're saving for, and why you need others to help
("Motivation"). Also enter the e-mail address of your PayPal account. Select the
currency for the goal, and how much you need. Optionally, also enter how much
money you have already saved. Finally enter a "Thank you Message", which is
shown after someone makes a donation.

Pay special attention to the Currency field! Once you received your first
donation through PayPal, this can no longer be changed!

Views Integration:
------------------
Donation Goals comes with very flexible options for Views integration. A default
view is available but disabled. You can enable it at Administer >
Site Building > Views. You can use this as a base to customize your donor
overviews with.

All the Donation Goal specific fields are available through Views as well.
Using a regular Node view you can add these to show donation goal specific
values. To add in the actual donations, add one or more
"Donation Goal Donations" fields.

If you need to get the account information of a donor (if he/she was logged in
while donating, or the email address was found with an account on the site),
you'll need to add a "Donation Goal Donations: User" Relationship to the view.
If you now add a field related to users, you can choose to use this
relationship. This will use the donation's user information instead of the
node author's information.

Themeing:
---------
This module adds two templates which you can copy and modify in your theme:
donation_goals_progress.tpl.php - This is the current progress that's shown in
 the teaser and full node view.
donation_goals_thankyou.tpl.php - This is the page which shows the thank-you
 message. To test this, go to "node/%/thankyou" (where % is the node number of a
 donation goal).

Related Modules:
----------------
The following modules offer similar or related functionality:

ChipIn: http://drupal.org/project/chipin (Not available for Drupal 5 or later)
This module adds similar functionality through a widget that you place in your
content. Donation Goals is much more integrated into your site, though.

Donation: http://drupal.org/project/donation
This module is to accept donations to your site, but not for specific goals.
Also uses the Simple Paypal Framework.

lm_paypal: http://drupal.org/project/lm_paypal
Provides tip jars (donations), subscriptions and paid adverts functionality
using PayPal.

Author:
-------
Wouter Haffmans (haffmans) - http://drupal.org/user/107698
