<?php

/**
 * @file
 * Administration form for donation_goals module.
 */

function donation_goals_admin() {
  $form = array();
  $form['donation_goals_info'] = array(
    '#title' => t('Information message'),
    '#type' => 'textarea',
    '#rows' => 5,
    '#default_value' => _donation_goals_donation_info_message(),
    '#required' => TRUE,
    '#description' => t("Enter a message to show on the donation form. It's recommended to write at least some basic info saying it may take some time before the donation is fully processed. Also mention that in some cases PayPal may charge a fee, and as such the net donated amount may be less than what was paid."),
  );
  $current_format = filter_resolve_format(variable_get('donation_goals_message_format', FILTER_FORMAT_DEFAULT));
  $form['donation_goals_message_format'] = filter_form($current_format);

  $form['donation_goals_currency_position'] = array(
    '#title' => t('Position of currency symbol'),
    '#type'  => 'select',
    '#options' => array(
      DONATION_GOALS_CURRENCY_PREFIX => t('Before'),
      DONATION_GOALS_CURRENCY_SUFFIX => t('After'),
      DONATION_GOALS_CURRENCY_HIDDEN => t('Do not show'),
    ),
    '#default_value' => variable_get('donation_goals_currency_position', 'prefix'),
    '#required' => TRUE,
    '#description' => t('Please note that this does not change the currency position of any (overridden) Views fields. You will need to change those occurrences manually if you change this setting. You may need to clear the cache at the Performance page before changes are visible everywhere.'),
  );

  return system_settings_form($form);
}