<?php
/**
 * @file
 * Field handler to show the PayPal name of a donor, optionally linking it to
 * the user's profile.
 */

/**
 * Field handler to show the PayPal name of a donor, optionally linking it to
 * the user's profile.
 */
class donation_goals_handler_field_donation_goal_donation_name extends views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['uid'] = 'uid';
  }

  function render($values) {
    $uid = $values->{$this->aliases['uid']};
    if ($uid > 0 && user_access('access user profiles')) {
      return l($values->{$this->field_alias}, "user/$uid");
    }
    else {
      return $values->{$this->field_alias};
    }
  }
}

