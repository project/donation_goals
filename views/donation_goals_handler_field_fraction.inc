<?php

/**
 * @file
 * Field handler for displaying the fraction of a total, defined by another field.
 * Offers options to show the number either as fraction or as percentage.
 * If the definition sets a "maximum" entry (as fraction), the value will be
 * limited to that value. E.g., setting a maximum of 1 will make a percentage
 * never exceed 100% (or 1.0 if shown as fraction).
 */

/**
 * Field handler for displaying the fraction of a total, defined by another field.
 * Offers options to show the number either as fraction or as percentage.
 * If the definition sets a "maximum" entry (as fraction), the value will be
 * limited to that value. E.g., setting a maximum of 1 will make a percentage
 * never exceed 100% (or 1.0 if shown as fraction).
 */
class donation_goals_handler_field_fraction extends views_handler_field_numeric {
  /**
   * Constructor to provide additional field to add. The total value is loaded
   * from the definition.
   */
  function construct() {
    parent::construct();
    $this->definition['float'] = TRUE;
    $this->additional_fields['total'] = $this->definition['total field'];
  }

  /**
   * Option definitions for percentage view: fraction or percentage,
   * precision (# decimal places) and in the case of percentage: do or do not show
   * percentage sign
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['type'] = array('default' => 'percentage');
    $options['percentage_sign'] = array('default' => TRUE, 'definition bool' => 'percentage_sign');

    return $options;
  }

  /**
   * Extend the options form with the format/precision/percentage_sign options
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['type'] = array(
      '#type' => 'select',
      '#title' => t('Output format'),
      '#options' => array(
        'percentage' => t('Percentage'),
        'precision' => t('Fraction'),
      ),
      '#default_value' => $this->options['type'],
      '#description' => t('Percentage format is shown as "42" or "42%" (depending on the "Show Percentage Sign" option below). Fraction is shown as "0.42", for example.'),
    );

    $form['percentage_sign'] = array(
      '#type' => 'checkbox',
      '#title' => t('Append percentage sign'),
      '#description' => t('If checked and the output format is Percentage, this will put the "%" sign after the number (before the suffix).'),
      '#default_value' => $this->options['percentage_sign'],
    );
  }

  /**
   * Render an amount (together with currency) through simple_paypal
   */
  function render($values) {
    $total = $values->{$this->aliases['total']};

    // Temporarily store suffix and restore it at the end
    $suffix = $this->options['suffix'];
    if (isset($total) && ($total != 0)) {
      $value = (float)(($values->{$this->field_alias}) / (float)($total));
      if (!empty($this->definition['maximum'])) {
        $value = min(array($value, $this->definition['maximum']));
      }

      // Percentage handling
      if ($this->options['type'] == 'percentage') {
        $value *= 100;
        if ($this->options['percentage_sign']) {
          $this->options['suffix'] = '% ' . $this->options['suffix'];
        }
      }
    }
    else {
      // Cannot divide by 0 -> just make value 0
      $value = 0;
    }

    // Render contents
    $original_value = $values->{$this->field_alias};
    $values->{$this->field_alias} = $value;
    $rendered = parent::render($values);
    $values->{$this->field_alias} = $original_value;
    $this->options['suffix'] = $suffix;
    return $rendered;
  }
}