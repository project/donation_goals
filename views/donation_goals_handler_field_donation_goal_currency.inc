<?php

/**
 * @file
 * Field handler to translate a currency into its full name form.
 */

/**
 * Field handler to translate a currency into its full name form. Uses simple_paypal
 * to get the names.
 */
class donation_goals_handler_field_donation_goal_currency extends views_handler_field_node {
  /**
   * Render a currency
   */
  function render($values) {
    $currencies = simple_paypal_get_currencies();
    $currency = $values->{$this->field_alias};
    $value = array_key_exists($currency, $currencies) ? $currencies[$currency] : $currency;
    return $this->render_link(t(check_plain($value)), $values);
  }
}