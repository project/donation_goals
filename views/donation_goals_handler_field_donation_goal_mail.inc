<?php

/**
 * @file
 * Field handler for a donation goal's PayPal email address.
 */

/**
 * Field handler for the donation goal's PayPal email address. Can be reused for other
 * email addresses, without having to attach them to a user.
 */
class donation_goals_handler_field_donation_goal_mail extends views_handler_field {
  /**
   * Define the "link_to_mail" (Make mailto: link) option
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['link_to_mail'] = array('default' => TRUE);
    return $options;
  }

  /**
   * Add the "Make mailto: Link" option.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['link_to_mail'] = array(
      '#title' => t('Make mailto: Link'),
      '#type' => 'checkbox',
      '#return_value' => TRUE,
      '#default_value' => $this->options['link_to_mail'],
    );
  }

  /**
   * Render the link or plain value, depending on the options
   */
  function render($values) {
    if ($this->options['link_to_mail']) {
      return l($values->{$this->field_alias}, "mailto:" . $values->{$this->field_alias});
    }
    return check_plain($values->{$this->field_alias});
  }
}
