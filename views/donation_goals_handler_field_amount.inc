<?php

/**
 * @file
 * Field handler for an donation goal amount.
 * This adds the currency symbol if known, as prefix, suffix or not at all (options).
 */

/**
 * Field handler for an donation goal amount.
 * This adds the currency symbol if known, as prefix, suffix or not at all (options).
 * It defaults to the site-wide setting for this. The reason for the somewhat
 * duplicate setting here, is to have Views more flexible and to allow other
 * modules to reuse this field with their own settings.
 *
 */
class donation_goals_handler_field_amount extends views_handler_field_numeric {
  /**
   * Constructor to provide additional field to add. In this case we need the currency
   * of the donation goal.
   */
  function construct() {
    parent::construct();
    $this->additional_fields['currency'] = (isset($this->definition['currency field']))
                                         ? $this->definition['currency field']
                                         : array(
                                             'table' => 'donation_goal',
                                             'field' => 'currency'
                                           );
  }

  /**
   * Option definitions for percentage view: show the currency symbol as prefix
   */
  function option_definition() {
    $options = parent::option_definition();

    $default_position = variable_get('donation_goals_currency_position', DONATION_GOALS_CURRENCY_PREFIX);

    $options['currency_symbol'] = array('default' => $default_position);

    return $options;
  }

  /**
   * Extend the options form with the format option
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['currency_symbol'] = array(
      '#type' => 'select',
      '#title' => t('Currency Symbol Position'),
      '#options' => array(
        DONATION_GOALS_CURRENCY_PREFIX => t('Prefix'),
        DONATION_GOALS_CURRENCY_SUFFIX => t('Suffix'),
        DONATION_GOALS_CURRENCY_HIDDEN => t('Do not show'),
      ),
      '#default_value' => $this->options['currency_symbol'],
    );
  }

  /**
   * Render an amount (together with currency)
   */
  function render($values) {
    // Set prefix depending on currency
    $symbol = donation_goals_currency_symbol($values->{$this->aliases['currency']});

    // Cache prefix/suffix options to restore them later (otherwise we'll get
    // double pre/suffix for the next row
    $prefix = $this->options['prefix'];
    $suffix = $this->options['suffix'];

    // Set symbol as pre/suffix depending on options
    switch ($this->options['currency_symbol']) {
      case DONATION_GOALS_CURRENCY_PREFIX:
        $this->options['prefix'] = (!empty($prefix)) ? "$prefix $symbol" : $symbol;
        break;
      case DONATION_GOALS_CURRENCY_SUFFIX:
        $this->options['suffix'] = (!empty($suffix)) ? "$symbol $suffix" : $symbol;
        break;
      default:
        break;
    }

    // Render value
    $rendered = parent::render($values);
    // Restore options
    $this->options['prefix'] = $prefix;
    $this->options['suffix'] = $suffix;

    return $rendered;
  }
}