<?php

/**
 * @file
 * Views data handler for donation_goals module.
 */

/**
 * Implementation of hook_views_data().
 */
function donation_goals_views_data() {
  // Donation goals
  $data = array();

  // Donation goals table
  $data['donation_goal']['table']['group'] = t('Donation Goals');
  $data['donation_goal']['table']['join'] = array(
    // Join on revision ID, not on node ID (because we keep revision information for donation goals)
    'node' => array(
      'left_field' => 'vid',
      'field' => 'vid',
    ),
    // Allow joining from individual donations by nid (thus through node and current vid)
    'donation_goal_donation' => array(
      'left_table' => 'node',
      'left_field' => 'vid',
      'field' => 'vid',
    ),
  );

  // Donations table
  $data['donation_goal_donation']['table']['group'] = t('Donation Goal Donations');
  // Join donation to node
  $data['donation_goal_donation']['table']['join'] = array(
    // Donations are per node (not per revision), so link this by nid
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  // -- Donation goal: data fields
  // Paypal Email address
  $data['donation_goal']['mail'] = array(
    'title'    => t('PayPal E-mail'),
    'help'     => t('The paypal e-mail address donations should be made to'),
    'field'    => array(
      'handler' => 'donation_goals_handler_field_donation_goal_mail',
      'click sortable' => TRUE,
    ),
    'filter'   => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort'     => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_argument_string',
    ),
  );

  // Currency
  $data['donation_goal']['currency'] = array(
    'title'    => t('Currency'),
    'help'     => t('The currency of a donation goal'),
    'field'    => array(
      'handler' => 'donation_goals_handler_field_donation_goal_currency',
      'click sortable' => TRUE,
    ),
    'filter'   => array(
      'handler' => 'donation_goals_handler_filter_donation_goal_currency',
    ),
    'sort'     => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'donation_goals_handler_argument_donation_goal_currency',
    ),
  );

  // Amount
  $data['donation_goal']['amount'] = array(
    'title'    => t('Amount'),
    'help'     => t('The target amount of a donation goal'),
    'field'    => array(
      'handler' => 'donation_goals_handler_field_amount',
      'click sortable' => TRUE,
    ),
    'filter'   => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort'     => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Amount saved
  $data['donation_goal']['amount_saved'] = array(
    'title'    => t('Amount saved'),
    'help'     => t('The amount of money saved so far'),
    'field'    => array(
      'handler' => 'donation_goals_handler_field_amount',
      'click sortable' => TRUE,
      'float' => TRUE,
    ),
    'filter'   => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort'     => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Amount saved percentage
  $data['donation_goal']['percentage'] = array(
    'title'    => t('Percentage Saved'),
    'help'     => t('Percentage of the total that was saved so far'),
    'field'    => array(
      'handler' => 'donation_goals_handler_field_fraction',
      'click sortable' => TRUE,
      'field' => 'amount_saved',

      'total field' => array(
        'field' => 'amount',
      ),
    ),
  );

  // Thank-you message
  $data['donation_goal']['thankyou'] = array(
    'title'    => t('Thank-you message'),
    'help'     => t('A brief message shown after making a donation.'),
    'field'    => array(
      'handler' => 'views_handler_field_markup',
      'format' => 'thankyou_format',
    ),
    'filter'   => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  // Thank-you message format (ID)
  $data['donation_goal']['thankyou_format'] = array(
    'title'    => t('Thank-you message Input format id'),
    'help'     => t("The numeric input format of the revision's thank-you message. !default means the default input format.", array('!default' => FILTER_FORMAT_DEFAULT)),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    // Information for sorting on input format
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    // Information for accepting input format as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  // -- Donation goal donation: data fields
  // Transaction ID
  $data['donation_goal_donation']['txnid'] = array(
    'title'    => t('Transaction ID'),
    'help'     => t('The PayPal transaction ID'),
    'field'    => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter'   => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort'     => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_argument_string',
    ),
  );

    // Timestamp
  $data['donation_goal_donation']['timestamp'] = array(
    'title'    => t('Time of donation'),
    'help'     => t('The date and time when the donation was made'),
    'field'    => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'filter'   => array(
      'handler' => 'views_handler_filter_date',
    ),
    'sort'     => array(
      'handler' => 'views_handler_sort_date',
    ),
  );

  // Timestamp aliases - we reuse those for the node creation
  $data['donation_goal_donation']['timestamp_fulldate'] = array(
    'title' => t('Donation date'),
    'help' => t('In the form of CCYYMMDD.'),
    'argument' => array(
      'field' => 'timestamp',
      'handler' => 'views_handler_argument_node_created_fulldate',
    ),
  );

  $data['donation_goal_donation']['timestamp_year_month'] = array(
    'title' => t('Donation year + month'),
    'help' => t('In the form of YYYYMM.'),
    'argument' => array(
      'field' => 'timestamp',
      'handler' => 'views_handler_argument_node_created_year_month',
    ),
  );

  $data['donation_goal_donation']['timestamp_year'] = array(
    'title' => t('Donation year'),
    'help' => t('In the form of YYYY.'),
    'argument' => array(
      'field' => 'timestamp',
      'handler' => 'views_handler_argument_node_created_year',
    ),
  );

  $data['donation_goal_donation']['timestamp_month'] = array(
    'title' => t('Donation month'),
    'help' => t('In the form of MM (01 - 12).'),
    'argument' => array(
      'field' => 'timestamp',
      'handler' => 'views_handler_argument_node_created_month',
    ),
  );

  $data['donation_goal_donation']['timestamp_day'] = array(
    'title' => t('Donation day'),
    'help' => t('In the form of DD (01 - 31).'),
    'argument' => array(
      'field' => 'timestamp',
      'handler' => 'views_handler_argument_node_created_day',
    ),
  );

  $data['donation_goal_donation']['timestamp_week'] = array(
    'title' => t('Donation week'),
    'help' => t('In the form of WW (01 - 53).'),
    'argument' => array(
      'field' => 'timestamp',
      'handler' => 'views_handler_argument_node_created_week',
    ),
  );
  // End of timestamp fields

  // User ID
  $data['donation_goal_donation']['uid'] = array(
      // uid field
    'title' => t('User'),
    'help' => t('Relate a donation to the user who made the donation.'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'field' => 'uid',
      'label' => t('user'),
    ),
  );

  // Payer PayPal name
  $data['donation_goal_donation']['name'] = array(
    'title'   => t('PayPal Name'),
    'help'    => t('The name (on PayPal) of the person or business who made the donation.'),
    'field'   => array(
      // Link to uid
      'handler' => 'donation_goals_handler_field_donation_goal_donation_name',
      'click sortable' => TRUE,
    ),
    'filter'   => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort'     => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_argument_string',
    ),
  );

  // Payer email address
  $data['donation_goal_donation']['mail'] = array(
    'title'    => t('PayPal E-mail'),
    'help'     => t('The paypal e-mail address the donation was made with'),
    'field'    => array(
      'handler' => 'donation_goals_handler_field_donation_goal_mail',
      'click sortable' => TRUE,
    ),
    'filter'   => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort'     => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_argument_string',
    ),
  );

  // Donation Amount
  $data['donation_goal_donation']['amount'] = array(
    'title'    => t('Amount'),
    'help'     => t('The amount of money donated'),
    'field'    => array(
      'handler' => 'donation_goals_handler_field_amount',
      'click sortable' => TRUE,
      'float' => TRUE,
    ),
    'filter'   => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort'     => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Percentage alias
  $data['donation_goal_donation']['percentage'] = array(
    'title'    => t('Percentage'),
    'help'     => t('Percentage of the total that was donated'),
    'field'    => array(
      'handler' => 'donation_goals_handler_field_fraction',
      'click sortable' => TRUE,
      'field' => 'amount',

      'total field' => array(
        'table' => 'donation_goal',
        'field' => 'amount',
      ),
    ),
  );

  return $data;
}