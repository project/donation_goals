<?php

/**
 * @file
 * Filter hanlder for currencies.
 */

/**
 * Filter by currency. Note that although we could do with just a
 * views_handler_filter_in_operator() and setting the optins callback in the definition,
 * this also allows to set a title.
 */
class donation_goals_handler_filter_donation_goal_currency extends views_handler_filter_in_operator {
  function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t('Currency');
      $this->value_options = simple_paypal_get_currencies();
    }
  }
}