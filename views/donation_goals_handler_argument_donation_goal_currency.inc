<?php

/**
 * @file
 * Argument handler for (simple_paypal supported) currencies, used by donation_goals.
 */

/**
 * Argument handler to accept a donation goal's currency (as long as supported by simple_paypal).
 */
class donation_goals_handler_argument_donation_goal_currency extends views_handler_argument {
  function construct() {
    parent::construct('currency');
  }

  /**
   * Override the behavior of summary_name(). Get the user friendly version
   * of the currency.
   */
  function summary_name($data) {
    return $this->currency($data->{$this->name_alias});
  }

  /**
   * Override the behavior of title(). Get the user friendly version of the
   * currency.
   */
  function title() {
    return $this->currency($this->argument);
  }

  /**
   * Get the currency name of a currency code (e.g. change "USD" into "U.S. Dollar",
   * or "EUR" into "Euro")
   */
  function currency($code) {
    $currencies = simple_paypal_get_currencies();
    $output = $currencies[$code];
    if (empty($output)) {
      return t($code);
    }
    else {
      return check_plain($output);
    }
  }
}
