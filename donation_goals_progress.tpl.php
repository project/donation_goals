<?php

/**
 * @file
 * Template for the progress block of a donation goal. The following variables
 * are available:
 * $node (object) - The node itself
 * $donation_goal (object) - The raw data of the goal
 * $teaser - Boolean to show whether the node is shown as teaser or not
 * $mail - The e-mail address to make donations to
 * $currency - The three-letter currency code of the goal
 * $currency_symbol - The symbol (if available) of the currency. May be the
 *                    three letter code if no symbol is known.
 * $currency_name - The full name of the currency
 * $amount - The target amount of money for the donation goal (with currency symbol)
 * $amount_saved - The amount of money saved to far (with currency symbol)
 * $percentage - The percentage of money (of the total) saved so far, with
 *               percentage character. This is without decimal digits.
 * $node_url - The URL of the node
 */
?>
<h4>Progress:</h4>
<p>
<?php if ($donation_goal->amount > 0): ?>
  <?php print $amount_saved; ?> of <?php print $amount; ?> (<?php print $percentage; ?>) saved so far.
<?php else: ?>
  <?php print $amount_saved; ?> of <?php print $amount; ?> saved so far.
<?php endif; ?>
</p>

<?php if ($donation_goal->amount > 0 && $donation_goal->amount_saved > $donation_goal->amount): ?>
<p>The target has been reached. Thank you for your generosity!</p>
<?php endif; ?>
